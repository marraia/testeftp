﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly string _caminhoBase;
        private readonly string _usuarioFtp;
        private readonly string _senhaFtp;
        private readonly string _servidorBase;

        public ValuesController(IConfiguration configuration)
        {
            _caminhoBase = configuration.GetSection("CaminhoUploadBase").Value;
            _usuarioFtp = configuration.GetSection("UsuarioFTP").Value;
            _senhaFtp = configuration.GetSection("SenhaFTP").Value;
            _servidorBase = configuration.GetSection("ServidorBaseImagem").Value;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost]
        [Route("ImagemPerfil")]
        [ProducesResponseType(typeof(string), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [RequestSizeLimit(500000)]
        public async Task<IActionResult> PostImagem(IFormFile imagem)
        {
            var pasta = "images";

            using (var stream = imagem.OpenReadStream())
            {
                var nomeNovoArquivo = TrocarNomeArquivo(imagem.FileName);
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"{_caminhoBase}/{pasta}/{nomeNovoArquivo}");
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential($@"{_usuarioFtp}", _senhaFtp);

                using (var ftpStream = request.GetRequestStream())
                    await imagem.CopyToAsync(ftpStream);


                return Ok($"{ _servidorBase}/{ pasta}/{ nomeNovoArquivo}");
            }
        }

        private string TrocarNomeArquivo(string nomeOriginal)
        {
            var randomFile = new Random().Next(1, 1000).ToString();
            return $"{randomFile}_{nomeOriginal}";
        }
    }
}
